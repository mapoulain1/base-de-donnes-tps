# Base de données - Maxime POULAIN - TP1
## Question 1 - SQL*Loader


### Création de la table

```sql
create table Auteurs(
    num number,
    nom varchar(50),
    prenom varchar(50),
    pays varchar(5),
    tel varchar(10)
);

create table Ouvrages(
    code number,
    titre varchar(50),
    prix number
);
```
Résultat : 
```
Table creee.
Table creee.
```


### Chargement

```sql
load data infile *
into table Auteurs
fields terminated by "/"
(num, nom, prenom nullif prenom='null', pays nullif pays='null',tel nullif tel='null')
begindata
1/Dupont/Jacques/FR/0473151585
2/Dupont/Marie/GB/null
3/Dupont/Pierre/null/null
3/Dupont/null/null/null
```

Résultat : 
```
$ sqlldr mapoulain1/******** auteurs.ctl

Table AUTEURS:
  4 Rows successfully loaded.
Check the log file:
  auteurs.log
```


```sql
insert into Ouvrages values (001,'Intro aux BD', 260);
insert into Ouvrages values (002,'Journal de Bolivie', null);
insert into Ouvrages values (003,'L''homme aux sandales', null);


1 ligne creee.
1 ligne creee.
1 ligne creee.
```

## Question 2 - Alter table (1)

```
@/opt/oracle/product/18c/dbhome_1/rdbms/admin/utlexcpt.sql

Table creee.

RENAME exceptions TO aut_violation;

```
```sql
alter table Auteurs add constraint pk_auteurs primary key(num) exceptions into aut_violation;

ORA-02437: impossible de valider (MAPOULAIN1.PK_AUTEURS) - violation de la cle primaire
```
Dans la table d'exceptions : 
```sql
ROW_ID             OWNER      TABLE_NAME    CONSTRAINT
------------------ ---------- ------------- ----------
AABg93AANAAAUbFAAC MAPOULAIN1 AUTEURS       PK_AUTEURS
AABg93AANAAAUbFAAD MAPOULAIN1 AUTEURS       PK_AUTEURS
```

On regarde quelles sont les lignes qui posent problème :
```sql
select * from Auteurs where rowid in (select row_id from exceptions);

NUM NOM    PRENOM PAYS TEL
--- ------ ------ ---- ---
3   Dupont Pierre
3   Dupont
```

On modifie les lignes qui ne remplissent pas la contrainte : 
```sql
update Auteurs set num = 4 where rowid='AABg93AANAAAUbFAAD';

1 ligne mise a jour.
```


## Question 3 - Alter table (2)
```sql
alter table Auteurs add constraint upper_check_auteurs check(upper(nom)=nom) exceptions into aut_violation;

ORA-02293: impossible de valider (MAPOULAIN1.UPPER_CHECK_AUTEURS) - violation d'une contrainte de controle
```
Dans la table d'exceptions : 

```sql
ROW_ID             OWNER      TABLE_NAME    CONSTRAINT
------------------ ---------- ------------- ----------
AABg93AANAAAUbFAAA MAPOULAIN1 AUTEURS       UPPER_CHECK_AUTEURS
AABg93AANAAAUbFAAB MAPOULAIN1 AUTEURS       UPPER_CHECK_AUTEURS
AABg93AANAAAUbFAAC MAPOULAIN1 AUTEURS       UPPER_CHECK_AUTEURS
AABg93AANAAAUbFAAD MAPOULAIN1 AUTEURS       UPPER_CHECK_AUTEURS
```

On modifie les lignes qui ne remplissent pas la contrainte : 

```sql
update Auteurs set nom=upper(nom);
```


## Question 4 - Drop constraint
```sql
alter table Auteurs drop constraint pk_auteurs;

Table modifiee.
```

