create table SalIntervalle_poulain (job varchar2(9), lsal number(7,2), hsal number(7,2));
insert into SalIntervalle_poulain values ('ANALYST', 2500, 3000);
insert into SalIntervalle_poulain values ('CLERK', 900, 1300);
insert into SalIntervalle_poulain values ('MANAGER', 2400, 3000);
insert into SalIntervalle_poulain values ('PRESIDENT', 4500, 4900);
insert into SalIntervalle_poulain values ('SALESMAN', 1200, 1700);


create or replace function salok_poulain
(
    in_job SalIntervalle_poulain.job%type,
    in_salaire SalIntervalle_poulain.lsal%type
)
return number
is
    jobCount number;
begin
    select count(*) into jobCount from SalIntervalle_poulain where in_job = job;
    if(jobCount = 0) then
        raise_application_error(-20000,'le job n''existe pas');
    else
        select count(*) into jobCount from SalIntervalle_poulain where in_job = job and lsal <= in_salaire and hsal >= in_salaire;
        return jobCount;
    end if;
end;
/

select salok_poulain('CLERK',1500) as RES from dual;
select salok_poulain('CLERK',1100) as RES from dual;
select salok_poulain('NOT MY JOB',1100) as RES from dual;