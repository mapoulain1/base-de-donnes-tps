create or replace procedure raisesalary_poulain
(
    in_empid emp.empno%type,
    amount emp.sal%type
)
is
    empCount number;
begin
    if(amount <= 0) then
        raise_application_error(-20001,'L''augmentation ne peut pas etre negative.');
    end if;
    
    select count(*) into empCount from emp where in_empid = empno;
    if(empCount = 0) then
        raise_application_error(-20000,'L''employe n''existe pas.');
    else
        update emp set sal=sal+amount where empno = in_empid;
    end if;
end;
/

exec raisesalary_poulain(7000,3000);