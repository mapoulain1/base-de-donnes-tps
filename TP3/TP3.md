# Base de données - Maxime POULAIN - TP3

# A. Procédures et fonctions stockés

## Question 1 : Procédure createdept_poulain

```sql
create or replace procedure create_dep_poulain
(
    dep_no in dept.deptno%type,
    dep_name in dept.dname%type,
    dep_loc in dept.loc%type
)
is
    countDep number;
begin
    select count(*) into countDep from dept where deptno = dep_no;
    if (countDep > 0) then
        raise_application_error(-20000,'le departement existe deja.');
    else
        insert into dept values(dep_no,dep_name,dep_loc);
    end if;
end create_dep_poulain;
/
```

```sql
exec create_dep_poulain(50,'BIBI','BILLOM');
Procedure PL/SQL terminee avec succes.     
```

Une nouvelle fois : 


```sql
exec create_dep_poulain(50,'BOUBOU','BILLOM');
ERREUR a la ligne 1 : 
ORA-20000: le departement existe deja. 
ORA-06512: a "MAPOULAIN1.CREATE_DEP_POULAIN", ligne 12  
ORA-06512: a ligne 1  
```

## Question 2 : Fonction salok_poulain

```sql
create or replace function salok_poulain
(
    in_job SalIntervalle_poulain.job%type,
    in_salaire SalIntervalle_poulain.lsal%type
)
return number
is
    jobCount number;
begin
    select count(*) into jobCount from SalIntervalle_poulain where in_job = job;
    if(jobCount = 0) then
        raise_application_error(-20000,'le job n''existe pas');
    else
        select count(*) into jobCount from SalIntervalle_poulain where in_job = job and lsal <= in_salaire and hsal >= in_salaire;
        return jobCount;
    end if;
end;
/
```

On utilise la table DUAL pour appeler la fonction facilement.


Un exemple hors intervalle : 
```sql
select salok_poulain('CLERK',1500) as RES from dual;
     RES
--------
       0
```


Un exemple dans l'intervalle : 
```sql
select salok_poulain('CLERK',1100) as RES from dual;
     RES
--------
       1
```


Un exemple avec un job inconnu : 
```sql
select salok_poulain('NOT MY JOB',1100) as RES from dual;
ERREUR a la ligne 1 :
ORA-20000: le job n'existe pas
ORA-06512: a "MAPOULAIN1.SALOK_POULAIN", ligne 12 
```


## Question 3 : Procédure raisesalary_poulain

```sql
create or replace procedure raisesalary_poulain
(
    in_empid emp.empno%type,
    amount emp.sal%type
)
is
    empCount number;
begin
    if(amount <= 0) then
        raise_application_error(-20001,'L''augmentation ne peut pas etre negative.');
    end if;
    
    select count(*) into empCount from emp where in_empid = empno;
    if(empCount = 0) then
        raise_application_error(-20000,'L''employe n''existe pas.');
    else
        update emp set sal=sal+amount where empno = in_empid;
    end if;
end;
/
```

Test avec un employé connu pour passer de 3500 à 6500 :
```sql
7000 Poulain SALESMAN 7566 17/12/80 3500 20 

exec raisesalary_poulain(7000,3000); 
Procedure PL/SQL terminee avec succes. 

7000 Poulain SALESMAN 7566 17/12/80 6500 20 
```

Test avec un employé connu et une augmentation négative :
```sql
exec raisesalary_poulain(7000,-3000); 

ERREUR a la ligne 1 : 
ORA-20001: L'augmentation ne peut pas etre negative.  
ORA-06512: a "MAPOULAIN1.RAISESALARY_POULAIN", ligne 10  
ORA-06512: a ligne 1 
```

Test avec un employé inconnu :
```sql
exec raisesalary_poulain(666,3000); 

ERREUR a la ligne 1 : 
ORA-20000: L'employe n'existe pas.   
ORA-06512: a "MAPOULAIN1.RAISESALARY_POULAIN", ligne 15
ORA-06512: a ligne 1 
```





# B. Bloc PL/SQL
```sql
set serveroutput on;
declare 
    Cursor c is select table_name from cat;
    Cursor c_old is select table_name from cat where table_name like '%_OLD';
begin
    for row_c_old in c_old
    loop
        execute immediate 'drop table ' || row_c_old.table_name || ' purge';
        dbms_output.put_line('dropping table ' || row_c_old.table_name);
    end loop;

    for row_c in c
    loop
        execute immediate 'create table ' || row_c.table_name || '_OLD as select * from ' || row_c.table_name;
        dbms_output.put_line('creating table ' || row_c.table_name || '_OLD');
    end loop;
end;
/
```

À la première éxecution : 
```sql
creating table POULAIN_OLD
```

À la deuxième éxecution : 
```sql
dropping table POULAIN_OLD
creating table POULAIN_OLD
```



