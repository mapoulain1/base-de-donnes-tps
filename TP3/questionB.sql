set serveroutput on;
declare 
    Cursor c is select table_name from cat;
    Cursor c_old is select table_name from cat where table_name like '%_OLD';
begin
    for row_c_old in c_old
    loop
        execute immediate 'drop table ' || row_c_old.table_name || ' purge';
        dbms_output.put_line('dropping table ' || row_c_old.table_name);
    end loop;

    for row_c in c
    loop
        execute immediate 'create table ' || row_c.table_name || '_OLD as select * from ' || row_c.table_name;
        dbms_output.put_line('creating table ' || row_c.table_name || '_OLD');
    end loop;
end;
/

Create table poulain (id number(5));
insert into poulain Values (1);
insert into poulain Values (2);
insert into poulain Values (3);
