create or replace procedure create_dep_poulain
(
    dep_no in dept.deptno%type,
    dep_name in dept.dname%type,
    dep_loc in dept.loc%type
)
is
    countDep number;
begin
    select count(*) into countDep from dept where deptno = dep_no;
    if (countDep > 0) then
        raise_application_error(-20000,'le departement existe deja.');
    else
        insert into dept values(dep_no,dep_name,dep_loc);
    end if;
end create_dep_poulain;
/

exec create_dep_poulain(50,'BIBI','BILLOM');