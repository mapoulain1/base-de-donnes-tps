# Base de données - Maxime POULAIN - TP2
## Question 0 - Execution du script

```sql
@/home/userXX/Downloads/scriptTP1.sql

insert into emp values (7000,'Poulain','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),3500,NULL,20);
commit;


1 ligne creee.
Validation effectuee.
```

## Question 1 - MILLER

```sql
set serveroutput on;
declare
    Cursor c is select ename, sal, comm, dname
    from emp E
    join dept D on E.deptno = D.deptno
    where ename = 'MILLER';
    
    nom emp.ename%type;
    salaire emp.sal%type;
    commission emp.comm%type;
    nomDept dept.dname%type;
begin
    for row_c in c
    loop
        nom := row_c.ename;
        salaire := row_c.sal;
        commission := row_c.comm;
        nomDept := row_c.dname;

        dbms_output.put_line(
           'nom ' || nom 
        || ', salaire ' || salaire
        || ', commission ' || nvl(commission,0) 
        || ', departement ' || nomDept);
    
    end loop;
end;    
/
```

```sql
nom MILLER, salaire 1300, commission , departement ACCOUNTING

Procedure PL/SQL terminee avec succes.
```

## Question 2 - Valeurs dans Temp

```sql
set serveroutput on;
declare
begin
   for i in 1..100
   loop
        insert into temp values(
            i,
            i*100,
            i || case when MOD(i,2)=0 then ' est paire' else ' est impaire' end
        );
   end loop;
end;    
/
```
```sql
Procedure PL/SQL terminee avec succes.

  NUM_COL1   NUM_COL2   CHAR_COL
  --------   --------   --------
         1        100   1 est impaire
         2        200   2 est paire
         3        300   3 est impaire
         4        400   4 est paire
         5        500   5 est impaire
         6        600   6 est paire
         7        700   7 est impaire
         8        800   8 est paire
         9        900   9 est impaire
        10       1000   10 est paire
        11       1100   11 est impaire

```

Utilisation de l'opérateur ternaire (case when ... then ... else ... end).

## Question 3 - 5 meilleurs salaires


```sql
set serveroutput on;
declare
    Cursor c is select * from (select * from emp order by sal desc) where rownum <= 5;
begin
   for row_c in c
   loop
        insert into temp values(row_c.sal, row_c.empno, row_c.ename);
   end loop;
end;    
/
```

```sql
Procedure PL/SQL terminee avec succes.

NUM_COL1   NUM_COL2   CHAR_COL
--------   --------   --------
    5000       7839   KING
    3500       7000   Poulain
    3000       7788   SCOTT
    3000       7902   FORD
    2975       7566   JONES
```


## Question 4 - Salaires supérieurs à 2000$

```sql
set serveroutput on;
declare
    Cursor c is select * from emp where sal + nvl(comm,0) > 2000;
begin
   for row_c in c
   loop
        insert into temp values(row_c.sal + nvl(row_c.comm,0), row_c.empno, row_c.ename);
   end loop;
end;    
/
```

```sql
NUM_COL1   NUM_COL2    CHAR_COL
--------   --------    --------
     2975       7566   JONES
     2650       7654   MARTIN
     2850       7698   BLAKE
     2450       7782   CLARK
     3000       7788   SCOTT
     5000       7839   KING
     3000       7902   FORD
     3500       7000   Poulain
```

Utilisation de nvl(column, value) pour la commission.

## Question 5 - Manager avec salaire supérieur à 4000$

```sql
set serveroutput on;
declare
    Cursor c is select * from emp start with empno=7902 connect by prior mgr=empno;
begin
   for row_c in c
   loop
        if row_c.sal > 4000 or row_c.mgr is null then
            insert into temp values(row_c.sal, row_c.empno, row_c.ename);
            exit;
        end if;
   end loop;
end;    
/
```

```sql
Procedure PL/SQL terminee avec succes.

NUM_COL1    NUM_COL2    CHAR_COL
--------    --------    --------
    5000        7839    KING
```

Utilisation du start with ... connect by prior ... .

