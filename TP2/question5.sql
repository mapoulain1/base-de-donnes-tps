set serveroutput on;
declare
    Cursor c is select * from emp start with empno=7902 connect by prior mgr=empno;
begin
   for row_c in c
   loop
        if row_c.sal > 4000 or row_c.mgr is null then
            insert into temp values(row_c.sal, row_c.empno, row_c.ename);
            exit;
        end if;
   end loop;
end;    
/