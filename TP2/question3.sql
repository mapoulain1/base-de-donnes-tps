set serveroutput on;
declare
    Cursor c is select * from (select * from emp order by sal desc) where rownum <= 5;
begin
   for row_c in c
   loop
        insert into temp values(row_c.sal, row_c.empno, row_c.ename);
   end loop;
end;    
/