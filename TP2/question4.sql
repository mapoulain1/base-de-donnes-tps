set serveroutput on;
declare
    Cursor c is select * from emp where sal + nvl(comm,0) > 2000;
begin
   for row_c in c
   loop
        insert into temp values(row_c.sal + nvl(row_c.comm,0), row_c.empno, row_c.ename);
   end loop;
end;    
/