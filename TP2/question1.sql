set serveroutput on;
declare
    Cursor c is select ename, sal, comm, dname
    from emp E
    join dept D on E.deptno = D.deptno
    where ename = 'MILLER';
    
    nom emp.ename%type;
    salaire emp.sal%type;
    commission emp.comm%type;
    nomDept dept.dname%type;
begin
    for row_c in c
    loop
        nom := row_c.ename;
        salaire := row_c.sal;
        commission := row_c.comm;
        nomDept := row_c.dname;
        dbms_output.put_line('nom ' || nom || ', salaire ' || salaire || ', commission ' || commission || ', departement ' || nomDept);
    end loop;
end;    
/