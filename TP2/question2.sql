set serveroutput on;
declare
begin
   for i in 1..100
   loop
        insert into temp values(
            i,
            i*100,
            i || case when MOD(i,2)=0 then ' est paire' else ' est impaire' end
        );
   end loop;
end;    
/