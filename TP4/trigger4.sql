set serveroutput on;
create or replace trigger noweek_poulain 
before insert or update or delete on emp for each row
declare
    dayOfTheWeek number;
begin
    select TO_NUMBER(TO_CHAR(sysdate, 'D')) into dayOfTheWeek from dual;
    if(dayOfTheWeek = 6 or dayOfTheWeek = 7) then
        raise_application_error(-20000,'Impossible de modifer la table employe le weekend');
    end if;
end;
/

update emp set ename='The boss' where empno=9999;