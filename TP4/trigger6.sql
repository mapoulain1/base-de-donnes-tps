set serveroutput on;
create or replace trigger checksal_poulain before update of job on emp for each row
declare
    low_sal emp.sal%type;
    high_sal emp.sal%type;
begin
    if :old.job != 'PRESIDENT' then
        select lsal, hsal into low_sal, high_sal from salintervalle_poulain where job = :new.job;
        :new.sal:=greatest(low_sal, least(high_sal,:old.sal + 100));
        dbms_output.put_line('Nouveau salaire est : ' || :new.sal);
    end if;
end;
/

update emp set job='PRESIDENT' where empno=7839;
update emp set job='MANAGER' where empno=7934;
update emp set job='MANAGER' where empno=7369;

update emp set sal='1300' where empno=7934;



JOB             LSAL       HSAL
--------- ---------- ----------
ANALYST         2500       3000
CLERK            900       1300
MANAGER         2400       3000
PRESIDENT       4500       4900
SALESMAN        1200       1700