create or replace package poulain_package as
    cursor emp_par_dep_poulain(in_deptno emp.deptno%type) return emp%rowtype;
    procedure raisesalary_poulain(in_empid emp.empno%type,amount emp.sal%type);
    procedure afficher_emp_poulain(in_deptno emp.deptno%type);
end poulain_package;  
/

create or replace package body poulain_package as
    cursor emp_par_dep_poulain(in_deptno emp.deptno%type) return emp%rowtype is
        select * from emp where deptno = in_deptno;

    procedure raisesalary_poulain(in_empid emp.empno%type,amount emp.sal%type) is
        empCount number;
    begin
            if(amount <= 0) then
                raise_application_error(-20001,'L''augmentation ne peut pas etre negative.');
            end if;
            select count(*) into empCount from emp where in_empid = empno;
            if(empCount = 0) then
                raise_application_error(-20000,'L''employe n''existe pas.');
            else
                update emp set sal=sal+amount where empno = in_empid;
            end if;
    end;

    procedure afficher_emp_poulain(in_deptno emp.deptno%type) is
    begin
        for c_row in poulain_package.emp_par_dep_poulain(in_deptno)
        loop
            dbms_output.put_line('no : ' || c_row.empno || ' nom : ' || c_row.ename);
        end loop;
    end;

end poulain_package;
/
 
exec poulain_package.afficher_emp_poulain(20);
exec poulain_package.raisesalary_poulain(7000,200);