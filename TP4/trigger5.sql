create table stats_poulain(
    TypeMaj char(6),
    NbMaj number(5),
    DateDerniereMaj date
);


set serveroutput on;
create or replace trigger stats_trigger_poulain after insert or update or delete on emp for each row
declare
    type_maj varchar(6);
    nb_maj stats_poulain.NbMaj%type;
begin
    if inserting then type_maj := 'INSERT'; end if;
    if updating then type_maj := 'UPDATE'; end if;
    if deleting then type_maj := 'DELETE'; end if;
    begin 
        select NbMaj into nb_maj from stats_poulain where TypeMaj=type_maj;
        exception
        when no_data_found then
            nb_maj := 0;
            insert into stats_poulain values(type_maj, 0, sysdate);
    end;
    update stats_poulain set NbMaj=nb_maj+1, DateDerniereMaj=sysdate where TypeMaj=type_maj; 
end;
/

insert into emp values(6666,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6667,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6668,'BOSS','CLERK',null,'01/01/22',666,10,62);
update emp set ename='Test' where empno=6666;
update emp set ename='Test2' where empno=6666;
delete from emp where empno=6666 or empno=6667 or empno=6668;


set serveroutput on;
create or replace trigger stats_trigger_poulain after insert or update or delete on emp
declare
    type_maj varchar(6);
    nb_maj stats_poulain.NbMaj%type;
begin
    if inserting then type_maj := 'INSERT'; end if;
    if updating then type_maj := 'UPDATE'; end if;
    if deleting then type_maj := 'DELETE'; end if;
    begin 
        select NbMaj into nb_maj from stats_poulain where TypeMaj=type_maj;
        exception
        when no_data_found then
            nb_maj := 0;
            insert into stats_poulain values(type_maj, 0, sysdate);
    end;
    update stats_poulain set NbMaj=nb_maj+1, DateDerniereMaj=sysdate where TypeMaj=type_maj; 
end;
/