create or replace trigger raise_poulain before update of sal on emp for each row
begin
    if(:new.sal < :old.sal) then
        raise_application_error(-20000,'Le salaire ne peut pas etre diminue.');
    end if;
end;
/

update emp set sal=200 where empno=7000;