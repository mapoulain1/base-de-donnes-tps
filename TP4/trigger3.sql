set serveroutput on;
create or replace trigger dept_poulain before insert on emp for each row
declare 
    deptCount number;
    user_value_dname dept.dname%type;
    user_value_dloc dept.loc%type;
begin
    select count(*) into deptCount from dept where deptno=:new.deptno;
    if(deptCount = 0) then
        dbms_output.put_line('Departement innexistant, ajout dans la base.');
        insert into dept values(:new.deptno, 'A SAISIR', 'A SAISIR');
    end if;
end;
/



insert into emp values(9999,'BOSS','CLERK',null,'01/01/22',666,10,62);

delete from emp where deptno=62;