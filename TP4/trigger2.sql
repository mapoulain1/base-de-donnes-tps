create or replace trigger numdept_poulain before insert on dept for each row
begin
    if(:new.deptno < 61 or :new.deptno > 70) then
        raise_application_error(-20000,'Numero de departement doit etre entre 61 et 69 compris.');
    end if;
end;
/