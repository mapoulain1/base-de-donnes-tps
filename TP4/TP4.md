# Base de données - Maxime POULAIN - TP4

# A. Package

L'interface du package : 
```sql
create or replace package poulain_package as
    cursor emp_par_dep_poulain(in_deptno emp.deptno%type) return emp%rowtype;
    procedure raisesalary_poulain(in_empid emp.empno%type,amount emp.sal%type);
    procedure afficher_emp_poulain(in_deptno emp.deptno%type);
end poulain_package;  
/
```

L'implémentation du package : 
```sql

create or replace package body poulain_package as
    cursor emp_par_dep_poulain(in_deptno emp.deptno%type) return emp%rowtype is
        select * from emp where deptno = in_deptno;

    procedure raisesalary_poulain(in_empid emp.empno%type,amount emp.sal%type) is
        empCount number;
    begin
            if(amount <= 0) then
                raise_application_error(-20001,'L''augmentation ne peut pas etre negative.');
            end if;
            select count(*) into empCount from emp where in_empid = empno;
            if(empCount = 0) then
                raise_application_error(-20000,'L''employe n''existe pas.');
            else
                update emp set sal=sal+amount where empno = in_empid;
            end if;
    end;

    procedure afficher_emp_poulain(in_deptno emp.deptno%type) is
    begin
        for c_row in poulain_package.emp_par_dep_poulain(in_deptno)
        loop
            dbms_output.put_line('no : ' || c_row.empno || ' nom : ' || c_row.ename);
        end loop;
    end;

end poulain_package;
/
```


Quelques tests pour s'assurer que cela fonctionne : 
```sql
exec poulain_package.afficher_emp_poulain(20);

no : 7369 nom : SMITH 
no : 7566 nom : JONES 
no : 7788 nom : SCOTT
no : 7876 nom : ADAMS
no : 7902 nom : FORD
no : 7000 nom : Poulain
```


```sql
exec poulain_package.raisesalary_poulain(7000,200);

7000 Poulain SALESMAN 7566 17/12/80 6700 20   
```
Le salaire est passé de 6500 à 6700 pour l'employé 7000.



# A. Triggers
## Question 1 : Le salaire d’un employé ne diminue jamais

```sql
create or replace trigger raise_poulain before update of sal on emp for each row
begin
    if(:new.sal < :old.sal) then
        raise_application_error(-20000,'Le salaire ne peut pas etre diminue.');
    end if;
end;
/
```

Un test avec un salaire plus grand : 
```sql
update emp set sal=20000 where empno=7000;

1 ligne mise a jour.
```

Un test avec un salaire plus petit : 
```sql
update emp set sal=200 where empno=7000;

ERREUR a la ligne 1 :
ORA-20000: Le salaire ne peut pas etre diminue. 
ORA-06512: a "MAPOULAIN1.RAISE_POULAIN", ligne 3 
ORA-04088: erreur lors d'execution du declencheur 'MAPOULAIN1.RAISE_POULAIN'
```


## Question 2 : Le numéro de département doit être entre 61 et 69


```sql
create or replace trigger numdept_poulain before insert on dept for each row
begin
    if(:new.deptno < 61 or :new.deptno > 70) then
        raise_application_error(-20000,'Numero de departement doit etre entre 61 et 69 compris.');
    end if;
end;
/
```

Un test avec un bon numéro : 
```sql
insert into dept values(69,'THE TEST','LOCATION');

1 ligne creee.
```

Un test avec un mauvais numéro : 
```sql
insert into dept values(99,'THE TEST','LOCATION'); 

ERREUR a la ligne 1 :
ORA-20000: Numero de departement doit etre entre 61 et 69 compris.
ORA-06512: a "MAPOULAIN1.NUMDEPT_POULAIN", ligne 3
ORA-04088: erreur lors d'execution du declencheur 'MAPOULAIN1.NUMDEPT_POULAIN'
```

## Question 3 : Insertion d'un employé avec département inconnue


```sql
set serveroutput on;
create or replace trigger dept_poulain before insert on emp for each row
declare 
    deptCount number;
    user_value_dname dept.dname%type;
    user_value_dloc dept.loc%type;
begin
    select count(*) into deptCount from dept where deptno=:new.deptno;
    if(deptCount = 0) then
        dbms_output.put_line('Departement innexistant, ajout dans la base.');
        insert into dept values(:new.deptno, 'A SAISIR', 'A SAISIR');
    end if;
end;
/
```

Ajout d'un employé avec un département innexistant. On utilise la conversion automatique de date de SQL avec le format 'DD/MM/YYYY'.
```sql
insert into emp values(9999, 'BOSS', 'CLERK', null, '01/01/22', 666, 10, 62);
Departement innexistant, ajout dans la base.
```

On regarde si le département a été ajouté :
```sql
select * from dept;
DEPTNO DNAME          LOC
------ -------------- -------------
    10 ACCOUNTING     NEW YORK
    20 RESEARCH       DALLAS
    30 SALES          CHICAGO
    40 BUBU           CLERMONT
    50 BIBI           BILLOM
    69 THE TEST       LOCATION
    62 A SAISIR       A SAISIR
```

## Question 4 : Pas de modification le weekend

On utilise to_char() avec sysdate et un format 'D' pour avoir le numéro du jour dans la semaine. Puis on le transforme en chiffre grâce à to_number();

```sql
set serveroutput on;
create or replace trigger noweek_poulain 
before insert or update or delete on emp for each row
declare
    dayOfTheWeek number;
begin
    select TO_NUMBER(TO_CHAR(sysdate, 'D')) into dayOfTheWeek from dual;
    if(dayOfTheWeek = 6 or dayOfTheWeek = 7) then
        raise_application_error(-20000,'Impossible de modifer la table employe le weekend');
    end if;
end;
/
```
On test ce trigger un samedi : 

```sql
update emp set ename='The boss' where empno=9999;

ERREUR a la ligne 1 :
ORA-20000: Impossible de modifer la table employe le weekend
ORA-06512: a "MAPOULAIN1.NOWEEK_POULAIN", ligne 6
ORA-04088: erreur lors d'execution du declencheur 'MAPOULAIN1.NOWEEK_POULAIN'
```


## Question 5 : Désactivation du trigger précédent

```sql
alter trigger noweek_poulain disable;

Declencheur modifie.
```

On reteste le tigger un samedi : 

```sql
update emp set ename='The boss' where empno=9999;

1 ligne mise a jour.
```

Le trigger est bien désactivé.

## Question 6 : Activation du trigger précédent

```sql
alter trigger noweek_poulain enable;

Declencheur modifie.
```

## Question 7 : Quelques statistiques

Création de la table pour stocker les statistiques : 
```sql
create table stats_poulain(
    TypeMaj char(6),
    NbMaj number(5),
    DateDerniereMaj date
);

Table creee.
```

### A : Mise à jour de la table emp

```sql
set serveroutput on;
create or replace trigger stats_trigger_poulain after insert or update or delete on emp for each row
declare
    type_maj varchar(6);
    nb_maj stats_poulain.NbMaj%type;
begin
    if inserting then type_maj := 'INSERT'; end if;
    if updating then type_maj := 'UPDATE'; end if;
    if deleting then type_maj := 'DELETE'; end if;
    begin 
        select NbMaj into nb_maj from stats_poulain where TypeMaj=type_maj;
        exception
        when no_data_found then
            nb_maj := 0;
            insert into stats_poulain values(type_maj, 0, sysdate);
    end;
    update stats_poulain set NbMaj=nb_maj+1, DateDerniereMaj=sysdate where TypeMaj=type_maj; 
end;
/
```

On test en modifant la table.

```sql
insert into emp values(6666,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6667,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6668,'BOSS','CLERK',null,'01/01/22',666,10,62);
update emp set ename='Test' where empno=6666;
update emp set ename='Test2' where empno=6666;
delete from emp where empno=6666 or empno=6667 or empno=6668;
```

On regarde la table de statistiques. 
```sql
select * from stats_poulain;
TYPEMA      NBMAJ DATEDERN
------ ---------- --------
INSERT          3 04/03/22
UPDATE          2 04/03/22
DELETE          3 04/03/22
```


### B : Sans le _for each row_

La différence se fait sur les requêtes qui vont affecter plusieurs lignes en même temps, comme le delete dans le test précédent.

```sql
insert into emp values(6666,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6667,'BOSS','CLERK',null,'01/01/22',666,10,62);
insert into emp values(6668,'BOSS','CLERK',null,'01/01/22',666,10,62);
update emp set ename='Test' where empno=6666;
update emp set ename='Test2' where empno=6666;
delete from emp where empno=6666 or empno=6667 or empno=6668;
```


On regarde la table de statistiques pour remarquer la différence. 
```sql
select * from stats_poulain;
TYPEMA      NBMAJ DATEDERN
------ ---------- --------
INSERT          3 04/03/22
UPDATE          2 04/03/22
DELETE          1 04/03/22
```

## Question 8 : Augmentation de salaire

On utilise la fonction _least_ pour avoir le minimun de deux valeurs.

```sql
set serveroutput on;
create or replace trigger checksal_poulain before update of job on emp for each row
declare
    low_sal emp.sal%type;
    high_sal emp.sal%type;
begin
    if :old.job != 'PRESIDENT' then
        select lsal, hsal into low_sal, high_sal from salintervalle_poulain where job = :new.job;
        :new.sal:=greatest(low_sal, least(high_sal,:old.sal + 100));
        dbms_output.put_line('Nouveau salaire est : ' || :new.sal);
    end if;
end;
/
```


```sql
select * from emp where empno=7369;

EMPNO ENAME   JOB  MGR HIREDATE  SAL COMM DEPTNO
----- ----- ----- ---- -------- ---- ---- ------
 7369 SMITH CLERK 7902 17/12/80  800          20
```

On met à jour plusieurs fois l'employé 7369 pour voir comment son salaire évolue.

```sql
update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 900

update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 1000

update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 1100

update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 1200

update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 1300

update emp set job='CLERK' where empno=7369;
Nouveau salaire est : 1300
```

On test le fait que le salaire se met dans au minimum de l'interval quand il est inférieur.

```sql
select * from emp where empno=7876;

EMPNO ENAME   JOB  MGR HIREDATE  SAL COMM DEPTNO
----- ----- ----- ---- -------- ---- ---- ------
 7876 ADAMS CLERK 7788 13/07/87 1100          20


update emp set job='MANAGER' where empno=7876;
Nouveau salaire est : 2400

update emp set job='MANAGER' where empno=7876;
Nouveau salaire est : 2500
```

On test avec le président.

```sql
select * from emp where job='PRESIDENT';
EMPNO ENAME      JOB  MGR HIREDATE  SAL COMM DEPTNO
----- ----- --------- --- -------- ---- ---- ------
 7839 KING  PRESIDENT     17/11/81 3000          10

update emp set job='MANAGER' where empno=7839;
1 ligne mise a jour.

select * from emp where job='PRESIDENT';
EMPNO ENAME      JOB  MGR HIREDATE  SAL COMM DEPTNO
----- ----- --------- --- -------- ---- ---- ------
 7839 KING  PRESIDENT     17/11/81 3000          10
```